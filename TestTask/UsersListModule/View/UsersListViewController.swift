//
//  ViewController.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import UIKit

class UsersListViewController: UIViewController {
    
    @IBOutlet weak var usersList: UITableView!
    
    var presenter: UsersListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Users" 
        usersList.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
}

extension UsersListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let userName = presenter.users?[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = userName?.name
        return cell
    }
}

extension UsersListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let userId = presenter.users?[indexPath.row].id else { return }
        presenter.tapOnUser(userId: userId)
    }
}

extension UsersListViewController: UsersListViewProtocol {
    func success() {
        usersList.reloadData()
    }
    
    func failure(error: Error) {
        print("\(error.localizedDescription)")
    }
}



