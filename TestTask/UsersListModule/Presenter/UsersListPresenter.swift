//
//  UsersListPresenter.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import Foundation

protocol UsersListViewProtocol: class {
    func success()
    func failure(error: Error)
}

protocol UsersListPresenterProtocol: class {
    var users: User? { get set }
    
    init(view: UsersListViewProtocol, networkManager: NetworkManagerProtocol, router: RouterProtocol)
    func tapOnUser(userId: Int)
    func getUsers()
}

class UsersListPresenter: UsersListPresenterProtocol {
    
    var users: User?
    weak var view: UsersListViewProtocol?
    var router: RouterProtocol?
    let networkManager: NetworkManagerProtocol!
    
    required init(view: UsersListViewProtocol, networkManager: NetworkManagerProtocol, router: RouterProtocol) {
        self.view = view
        self.networkManager = networkManager
        self.router = router
        getUsers()
    }
    
    func tapOnUser(userId: Int) {
        router?.showPhotos(userId: userId)
    }
    
    func getUsers() {
        networkManager.getUsers { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let users):
                    self.users = users
                    self.view?.success()
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
}


