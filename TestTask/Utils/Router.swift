//
//  Router.swift
//  TestTask
//
//  Created by Иван Гришечко on 11.02.2021.
//

import UIKit

protocol RouterMain {
    var navigationController: UINavigationController? { get set }
    var assemblyBuilder: AssemblyBuilderProtocol? { get set }
}

protocol RouterProtocol: RouterMain {
    func initialViewController()
    func showPhotos(userId: Int)
}

class Router: RouterProtocol {
    var navigationController: UINavigationController?
    var assemblyBuilder: AssemblyBuilderProtocol?
    
    init(navigationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
        self.navigationController = navigationController
        self.assemblyBuilder = assemblyBuilder
    }
    
    func initialViewController() {
        if let navigationController = navigationController {
            guard let usersListViewController = assemblyBuilder?.createUserList(router: self) else {
                return
            }
            navigationController.viewControllers = [usersListViewController]
        }
    }
    
    func showPhotos(userId: Int) {
        if let navigationController = navigationController {
            guard let photosCollectionViewController = assemblyBuilder?.createPhotosCollection(userId: userId, router: self) else {
                return
            }
            navigationController.pushViewController(photosCollectionViewController, animated: true)
        }
    }
}
