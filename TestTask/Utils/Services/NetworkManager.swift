//
//  NetworkManager.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import UIKit

protocol NetworkManagerProtocol {
    func getUsers(completionHandler: @escaping (Result <User, Error>) -> Void )
    func getAlbums(userId: Int, completionHandler: @escaping (Result <Album, Error>) -> Void)
    func getPhotos(albumId: Int, completionHandler: @escaping (Result <Photo, Error>) -> Void)
}

class NetworkManager: NetworkManagerProtocol {
    
    func getUsers(completionHandler: @escaping (Result<User, Error>) -> Void) {
        var urlConstructor = URLComponents()
        urlConstructor.scheme = "https"
        urlConstructor.host = "jsonplaceholder.typicode.com"
        urlConstructor.path = "/users"
        
        guard let url = urlConstructor.url else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            
            do {
                let users = try JSONDecoder().decode(User.self, from: data!)
                completionHandler(.success(users))
            } catch {
                completionHandler(.failure(error))
            }
        } .resume()
    }
    
    func getAlbums(userId: Int, completionHandler: @escaping (Result <Album, Error>) -> Void) {
        var urlConstructor = URLComponents()
        urlConstructor.scheme = "https"
        urlConstructor.host = "jsonplaceholder.typicode.com"
        urlConstructor.path = "/albums"
        urlConstructor.queryItems = [URLQueryItem(name: "userId", value: "\(userId)")]
        
        guard let url = urlConstructor.url else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            
            do {
                let albums = try JSONDecoder().decode(Album.self, from: data!)
                completionHandler(.success(albums))
            } catch {
                completionHandler(.failure(error))
            }
        } .resume()
    }
    
    func getPhotos(albumId: Int, completionHandler: @escaping (Result <Photo, Error>) -> Void) {
        var urlConstructor = URLComponents()
        urlConstructor.scheme = "https"
        urlConstructor.host = "jsonplaceholder.typicode.com"
        urlConstructor.path = "/photos/"
        urlConstructor.queryItems = [URLQueryItem(name: "albumId", value: "\(albumId)")]
        
        guard let url = urlConstructor.url else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            
            do {
                let photos = try JSONDecoder().decode(Photo.self, from: data!)
                completionHandler(.success(photos))
            } catch {
                completionHandler(.failure(error))
            }
        } .resume()
    }
}

