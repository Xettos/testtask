//
//  UIImageView+Extension.swift
//  TestTask
//
//  Created by Иван Гришечко on 12.02.2021.
//

import UIKit

class ImageCache {
    var imageCahce = NSCache<NSString, UIImage>()
}

extension UIImageView {
    
    func downloaded(from url: URL, cache: ImageCache, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        if let cacheImage = cache.imageCahce.object(forKey: url.absoluteString as NSString) {
            image = cacheImage
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                else { return }
                cache.imageCahce.setObject(image, forKey: url.absoluteString as NSString)
                DispatchQueue.main.async() { [weak self] in
                    self?.showLoading()
                    self?.image = image
                    self?.stopLoading()
                }
            }.resume()
        }
    }
}
