//
//  ModuleBuilder.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import UIKit

protocol AssemblyBuilderProtocol {
    func createUserList(router: RouterProtocol) -> UIViewController
    func createPhotosCollection(userId: Int, router: RouterProtocol) -> UIViewController
    
}

class AssemblyModuleBuilder: AssemblyBuilderProtocol {
    func createUserList(router: RouterProtocol) -> UIViewController {
        let networkManager = NetworkManager()
        let view = UsersListViewController()
        let presenter = UsersListPresenter(view: view, networkManager: networkManager, router: router)
        view.presenter = presenter
        return view
    }
    
    func createPhotosCollection(userId: Int, router: RouterProtocol) -> UIViewController {
        let networkManager = NetworkManager()
        let view = PhotosViewController()
        let presenter = PhotosPresenter(view: view, networkManager: networkManager, userId: userId)
        view.presenter = presenter
        return view
    }
}
