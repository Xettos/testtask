//
//  Photo.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import Foundation

// MARK: - PhotoElement
struct PhotoElement: Codable {
    let albumID, id: Int
    let title: String
    let url, thumbnailURL: String
    
    enum CodingKeys: String, CodingKey {
        case albumID = "albumId"
        case id, title, url
        case thumbnailURL = "thumbnailUrl"
    }
}

typealias Photo = [PhotoElement]
