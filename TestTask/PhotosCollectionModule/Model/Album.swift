//
//  Album.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import Foundation

// MARK: - AlbumElement
struct AlbumElement: Codable {
    let userID, id: Int
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title
    }
}

typealias Album = [AlbumElement]
