//
//  PhotosPresenter.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import Foundation

protocol PhotosViewProtocol: class {
    func success()
    func failure(error: Error)
}

protocol PhotosViewPresenterProtocol: class {
    var photos: Photo? {get set}
    init(view: PhotosViewProtocol, networkManager: NetworkManagerProtocol, userId: Int)
    func getPhotos()
    func getData()
}

class PhotosPresenter: PhotosViewPresenterProtocol {
    
    weak var view: PhotosViewProtocol?
    let networkManager: NetworkManagerProtocol!
    let userId: Int
    var photos: Photo?
    var albums: Album?
    
    required init(view: PhotosViewProtocol, networkManager: NetworkManagerProtocol, userId: Int) {
        self.view = view
        self.networkManager = networkManager
        self.userId = userId
        self.getData()
    }
    
    func getData() {
        self.networkManager.getAlbums(userId: self.userId) {[weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let albums):
                    self.albums = albums
                    self.getPhotos()
                    self.view?.success()
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
    
    func getPhotos() {
        guard let albumIds: [Int] = (self.albums?.map {$0.id}) else { return }
        
        self.networkManager.getPhotos(albumId: albumIds.first!) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let photos):
                    self.photos = photos
                    self.view?.success()
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
}


