//
//  PhotosCollectionViewCell.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!  
    
    func renderCell(photos: Photo, indexPath: IndexPath) {
        guard let photo = URL(string: photos[indexPath.row].url) else { return }
        let photoTitle = photos[indexPath.row].title
        let cacher = ImageCache()
        
        self.title.text = photoTitle
        self.imageView.downloaded(from: photo, cache: cacher)
        self.setupLayers()
    }
    
    func setupLayers() {
        self.layer.masksToBounds = false
        
        self.contentView.layer.cornerRadius = 5
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 1.5
    }
}
