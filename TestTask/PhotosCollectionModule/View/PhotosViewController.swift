//
//  PhotosViewController.swift
//  TestTask
//
//  Created by Иван Гришечко on 10.02.2021.
//

import UIKit

private let reuseId = "PhotosCollectionViewCell"

class PhotosViewController: UIViewController {
    
    @IBOutlet weak var photosCollection: UICollectionView!
    var presenter: PhotosViewPresenterProtocol!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: reuseId,bundle: nil)
        self.photosCollection.register(nib, forCellWithReuseIdentifier: reuseId)
        
        photosCollection.dataSource = self
        photosCollection.delegate = self
        
        self.navigationItem.title = "Photos"
        presenter.getData()
        photosCollection.reloadData()
    }
}

extension PhotosViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.photos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = photosCollection.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as? PhotosCollectionViewCell {
            guard let photos = presenter.photos else {
                return UICollectionViewCell()
            }
            cell.renderCell(photos: photos, indexPath: indexPath)
            return cell
        }
        return UICollectionViewCell()
    }
}

extension PhotosViewController: PhotosViewProtocol {
    func success() {
        photosCollection.reloadData()
    }
    
    func failure(error: Error) {
        print("\(error.localizedDescription)")
    }
}
